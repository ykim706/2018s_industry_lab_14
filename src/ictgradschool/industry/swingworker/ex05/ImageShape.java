package ictgradschool.industry.swingworker.ex05;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.concurrent.ExecutionException;

/**
 * A shape which is capable of loading and rendering images from files / Uris / URLs / etc.
 */
public class ImageShape extends Shape {

    private Image image;
    private LoadImage img;


    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, String fileName) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, new File(fileName).toURI());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URI uri) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, uri.toURL());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URL url) {
        super(x, y, deltaX, deltaY, width, height);

        img = new LoadImage(width, height, url);
        img.execute();

//        try {
//            Image image = ImageIO.read(url);
//            if (width == image.getWidth(null) && height == image.getHeight(null)) {
//                this.image = image;
//            } else {
//                this.image = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }

    public class LoadImage extends SwingWorker<Image, String> {

        private int width;
        private int height;
        private URL url;

        public LoadImage(int width, int height, URL url) {
            this.width = width;
            this.height = height;
            this.url = url;
        }

        @Override
        protected Image doInBackground() throws Exception {
            Image image = null;

            try {
                image = ImageIO.read(url);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (width == image.getWidth(null) && height == image.getHeight(null)) {
                return image;
            } else {
                image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
                return image;
            }

        }

        @Override
        protected void done() {
            try {
                Image i = get();
                ImageShape.this.image = i;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void paint(Painter painter) {
        if (image == null) {
            painter.setColor(Color.gray);
            painter.fillRect(fX, fY, fWidth, fHeight);
            painter.setColor(Color.white);
            painter.drawCenteredText("Loading image", fX, fY, fWidth, fHeight);
            painter.setColor(Color.red);
            painter.drawRect(fX, fY, fWidth, fHeight);

        }

        painter.drawImage(this.image, fX, fY, fWidth, fHeight);

    }
}
